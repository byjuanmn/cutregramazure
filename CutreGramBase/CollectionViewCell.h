//
//  CollectionViewCell.h
//  CutreGramBase
//
//  Created by Juan Antonio Martin Noguera on 21/01/15.
//  Copyright (c) 2015 cloudonmobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIImage *photo;
@end
