//
//  ViewController.h
//  CutreGramBase
//
//  Created by Juan Antonio Martin Noguera on 21/01/15.
//  Copyright (c) 2015 cloudonmobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>


@interface ViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;





@end

