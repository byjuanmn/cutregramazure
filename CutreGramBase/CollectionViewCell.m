//
//  CollectionViewCell.m
//  CutreGramBase
//
//  Created by Juan Antonio Martin Noguera on 21/01/15.
//  Copyright (c) 2015 cloudonmobile. All rights reserved.
//

#import "CollectionViewCell.h"

@interface CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *caption;

@end

@implementation CollectionViewCell


- (void)setPhoto:(UIImage *)photo{
    _photo = photo;
    self.imageView.image = photo;
    
}
@end
